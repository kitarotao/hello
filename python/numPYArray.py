import numpy as np
a = np.array([0, 1, 2, 3])
print(a)
#print(a.ndim)

b = np.array([[0, 1, 2], [3, 4, 5]])
print(b.shape)

c= np.ones((2,3,4),dtype=np.int16)
print (c.shape)

x = np.arange(0,50,2,dtype=np.int64)
print (x)
np.savetxt('test.out', x, delimiter=',')