import csv

# write list to CSV file, it works!
the_list=["car","house","wife"]
with open("my_file.csv", 'w') as outfile:
    writer = csv.writer(outfile, delimiter='\t')
    writer.writerow(the_list)


# read CSV file & load into list
with open("my_file.csv", 'r') as my_file:
    reader = csv.reader(my_file, delimiter='\t')
    my_list = list(reader)
    print(my_list[0])
