import csv
import time

columnMin = 0
columnMax = 0
numLine = 0 
lowestLine=0
highestLine=0

with open('tmp.txt') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=' ')
    for row in readCSV:
        #print(row)
        #print(row[0])
        #print(row[23],row[24],row[25],row[26],row[33])
        if numLine==0:
            columnMin=len((row))
            columnMax=len((row))
        
        if len((row))>columnMax:
            columnMax=len((row))
            highestLine=numLine
        #print(len(row))

        if len((row))<columnMin:
            columnMin=len((row))
            lowestLine=numLine
        
        numLine+=1
        

print("max column = " + str(columnMax))
print("min column = "+ str(columnMin))
print("num line = "+ str(numLine))
print("lowestLine = "+ str(lowestLine))
print("highestLine = "+ str(highestLine))