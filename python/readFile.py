#open a file for reading 
f= open("guru99.txt","r")
i=0 
if f.mode == "r":
    #use read() function to read an arguments
    #contents=f.read()
    #use readlines() function to read file line by line
    contents=f.readlines()


for row in contents:
    print('line no %d' %(i+1))
    print(row)
    i=i+1
#       f.write("This is line %d\r" %(i+1))

#close the  file when done
f.close()