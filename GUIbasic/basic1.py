from tkinter import *
from tkinter import messagebox

def mHello():
    print("Peng button")


def hello(event):
    #Dialog box
    messagebox.showinfo(title='แสดงข้อความ', message="Hello User !")
       

#screen setup
gui=Tk()
gui.geometry("450x450")
gui.title("Python GUI Basic")

#set up label and button
mlabel=Label(text="peng",fg="#000",bg="pink").pack()
#mlabel.pack()
mButton=Button(text="submit",fg ="red",bg="yellow",command=mHello).pack()
b1=Button(text="hello")
b1.bind('<Button-1>',hello)
b1.pack()
#setup textbox
objEntry=Entry().pack()
#setup menu
menubar=Menu(gui)
fileMenu=Menu(menubar,tearoff=0)
fileMenu.add_command(label="new")
fileMenu.add_command(label="open")
fileMenu.add_command(label="Save..As")
fileMenu.add_command(label="Close")
menubar.add_cascade(label="file",menu=fileMenu)


helpMenu=Menu(menubar,tearoff=0)
helpMenu.add_command(label="Contact")
helpMenu.add_command(label="Document")
menubar.add_cascade(label="Help",menu=helpMenu)

gui.config(menu=menubar)
gui.mainloop
